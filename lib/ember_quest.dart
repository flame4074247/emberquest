import 'package:flame/components.dart';
import 'package:flame/events.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';

import 'actors/ember.dart';
import 'actors/water_enemy.dart';
import 'managers/segment_manager.dart';
import 'objects/ground_block.dart';
import 'objects/platform_block.dart';
import 'objects/star.dart';
import 'overlays/hud.dart';

class EmberQuestGame extends FlameGame
  with HasCollisionDetection, HasKeyboardHandlerComponents{

  EmberQuestGame();

  late EmberPlayer _ember;
  double objectSpeed = 0.0;

  final world = World();
  late final CameraComponent cameraComponent;

  late double lastBlockXPosition = 0.0;
  late UniqueKey lastBlockKey;

  int starsCollected = 0;
  int health = 3;

  @override
  Future<void> onLoad() async {

    await images.loadAll([
        'block.png',
        'ember.png',
        'ground.png',
        'heart_half.png',
        'heart.png',
        'star.png',
        'water_enemy.png'
    ]);

    cameraComponent = CameraComponent(world: world);
    cameraComponent.viewfinder.anchor = Anchor.topLeft;
    addAll([cameraComponent, world]);

    initializeGame(true);
  }

  void loadGameSegments(int segmentIndex, double xPositionOffset){

    for(final block in segments[segmentIndex]){
      switch (block.blockType){
        case GroundBlock:
          add(GroundBlock(
            gridPosition: block.gridPosition,
            xOffset: xPositionOffset,
          ));
          break;
        case PlatformBlock:
          add(PlatformBlock(
            gridPosition: block.gridPosition,
            xOffset: xPositionOffset,
          ));
          break;
        case Star:
          add(Star(
            gridPosition: block.gridPosition,
            xOffset: xPositionOffset)
          );
          break;
        case WaterEnemy:
          add(WaterEnemy(
            gridPosition: block.gridPosition,
            xOffset: xPositionOffset
          ));
          break;
      }
    }
  }

  void initializeGame(bool loadHud){

    // Determine segments to load
    final segmentsToLoad = (size.x / 640).ceil();
    segmentsToLoad.clamp(0, segments.length);

    // Load segements
    for (var i = 0; i <= segmentsToLoad; i++) {
      loadGameSegments(i, (640 * i).toDouble());
    }

    // Instantiate ember
    _ember = EmberPlayer(position: Vector2(128, canvasSize.y - 128),);
    world.add(_ember);

    // Load HUD
    // cameraComponent.viewport.add(Hud());
    if (loadHud){
      add(Hud());
    }
  }

  @override
  void update(double dt){
    if (health <= 0){
      overlays.add('GameOver');
    }

    super.update(dt);
  }


  void reset(){
      starsCollected = 0;
    health = 3;
    initializeGame(false);
  }

  @override
  Color backgroundColor(){
    return const Color.fromARGB(255, 173, 223, 247);
  }

}