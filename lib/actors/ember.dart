import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flutter/services.dart';

import 'water_enemy.dart';
import '../ember_quest.dart';
import '../objects/ground_block.dart';
import '../objects/platform_block.dart';
import '../objects/star.dart';

class EmberPlayer extends SpriteAnimationComponent
  with CollisionCallbacks, KeyboardHandler, HasGameRef<EmberQuestGame>{

  // Movement
  int horizontalDirection = 0;  // direction of movement
  final Vector2 velocity = Vector2.zero();
  final double moveSpeed = 200;

  // Collision
  final Vector2 fromAbove = Vector2(0, -1);
  bool isOnGround = false;

  // Gravity / jumping
  final double gravity = 15;
  final double jumpSpeed = 1000;
  final double terminalVelocity = 500;

  bool hasJumped = false;

  // Hits
  bool hitByEnemy = false;


  EmberPlayer({
    required super.position,
  }) : super(size: Vector2.all(64), anchor: Anchor.center);

  @override
  void onLoad() {
    animation = SpriteAnimation.fromFrameData(
      game.images.fromCache('ember.png'),
      SpriteAnimationData.sequenced(amount: 4, stepTime: 0.12, textureSize: Vector2.all(16)),
    );

    add(
      CircleHitbox(),
    );
  }

  @override
  void update(double dt) {

    // Handle movement
    velocity.x = horizontalDirection * moveSpeed;

    // Handle character facing
    if (horizontalDirection < 0 && scale.x > 0) {
      flipHorizontally();
    }
    else if (horizontalDirection > 0 && scale.x < 0) {
      flipHorizontally();
    }

    // Apply basic gravity
    velocity.y += gravity;

    // Determine if ember has jumped 
    if (hasJumped) {
      if (isOnGround) {
        velocity.y = -jumpSpeed;
        isOnGround = false;
      }
      hasJumped = false;
    }

    // Prevent ember from jumping to crazy fast as well as descending too fast and 
    // crashing through the ground or a platform.
    velocity.y = velocity.y.clamp(-jumpSpeed, terminalVelocity);

    // Scrolling
    game.objectSpeed = 0;

    // Prevent ember from going backwards at screen edge.
    if (position.x - 36 <= 0 && horizontalDirection < 0) {
      velocity.x = 0;
      print('stop!');
    }

    // Prevent ember from going beyond half screen.
    if (position.x + 64 >= game.size.x / 2 && horizontalDirection > 0) {
      velocity.x = 0;
      game.objectSpeed = -moveSpeed;
    }

    // Position update based off velocity
    position += velocity * dt;

    // Handle falling in pit
    if (position.y > game.size.y + size.y) {
      game.health = 0;
    }

    // Handle zero health
    if (game.health <= 0){
      removeFromParent();
    }

    super.update(dt);
  }

  @override
  bool onKeyEvent(RawKeyEvent event, Set<LogicalKeyboardKey> keysPressed) {

    // No movemnt direction if nothing pressed
    horizontalDirection = 0;

    // Set direction to -1 if a or left arrow pressed
    horizontalDirection += (keysPressed.contains(LogicalKeyboardKey.keyA) ||
      keysPressed.contains(LogicalKeyboardKey.arrowLeft))
      ? -1
      : 0;

    // Set direction to 1 if d or left arrow pressed
    horizontalDirection += (keysPressed.contains(LogicalKeyboardKey.keyD) ||
      keysPressed.contains(LogicalKeyboardKey.arrowRight))
      ? 1
      : 0;

    //Jump
    hasJumped = keysPressed.contains(LogicalKeyboardKey.space);

    return true;
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
    if (other is GroundBlock || other is PlatformBlock) {
      if (intersectionPoints.length == 2) {
        // Calculate the collision normal and separation distance.
        final mid = (intersectionPoints.elementAt(0) +
          intersectionPoints.elementAt(1)) / 2;

        final collisionNormal = absoluteCenter - mid;
        final separationDistance = (size.x / 2) - collisionNormal.length;
        collisionNormal.normalize();

        // If collision normal is almost upwards,
        // ember must be on ground.
        if (fromAbove.dot(collisionNormal) > 0.9) {
          isOnGround = true;
        }

        // Resolve collision by moving ember along
        // collision normal by separation distance.
        position += collisionNormal.scaled(separationDistance);
        }
      }

      if (other is Star){
        print('Star!');
        other.removeFromParent();
        game.starsCollected ++;
      }

      if (other is WaterEnemy){
        print('Hit!');
        hit();
      }

    super.onCollision(intersectionPoints, other);
  }

  void hit(){
    if (!hitByEnemy){
      game.health --;
      hitByEnemy = true;
    }

    add(
      OpacityEffect.fadeOut(
        EffectController(
          alternate: true,
          duration: 0.1,
          repeatCount: 6,
        ),
      )..onComplete = () {
        hitByEnemy = false;
      },
    );
  }

}