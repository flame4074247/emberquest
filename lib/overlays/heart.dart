import 'package:ember_quest/ember_quest.dart';
import 'package:flame/components.dart';


enum HeartState {
  available,
  unavailable
}

class HeartHealthComponent extends SpriteGroupComponent<HeartState>
  with HasGameRef<EmberQuestGame> {
    
  final int heartNumber;
 
  HeartHealthComponent({
    required this.heartNumber,
    required super.position,
    required super.size,
    super.scale,
    super.angle,
    super.anchor,
    super.priority,
  });

  @override
  Future<void> onLoad() async {

    await super.onLoad();

    // Load sprites
    final availableSprite = await game.loadSprite(
      'heart.png',
      srcSize: Vector2.all(32),
    );

    final unavailableSprite = await game.loadSprite(
      'heart_half.png',
      srcSize: Vector2.all(32),
    );

    // Create sprite map of key value pairs. 
    sprites = {
      HeartState.available: availableSprite,
      HeartState.unavailable: unavailableSprite,
    };

    // Set default state
    current = HeartState.available;
  }

  @override
  void update(double dt) {

    // Check if heart is availabkle or not based on health in game
    if (game.health < heartNumber) {
      current = HeartState.unavailable;
    } else {
      current = HeartState.available;
    }

    super.update(dt);
  }

}